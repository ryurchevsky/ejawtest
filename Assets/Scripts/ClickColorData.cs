﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ClickColorData", menuName = "ClickColorData")]
public class ClickColorData : ScriptableObject
{
    public string ObjectType; //тип создаваемого объекта (куб, сфера, капсула)
    public int MinClicksCount;
    public int MaxClicksCount;
    public Color Color;
}
