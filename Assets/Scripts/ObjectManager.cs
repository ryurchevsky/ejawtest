﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class ObjectManager : MonoBehaviour
{
    public GeometryObjectData GeometryObjectData;
    public int clickCount = 0;
    public MeshRenderer mesh;
    public string tagName = "Geometry";

    private CompositeDisposable disposables;

    void Start()
    {
        UniRxTimer();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            ObjectClick();
    }

    public void ObjectClick()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.CompareTag(tagName))
            {
                clickCount++;

                foreach (var clicksData in GeometryObjectData.ClicksData)
                    if (clickCount >= clicksData.MinClicksCount && clickCount <= clicksData.MaxClicksCount)
                        mesh.material.color = clicksData.Color;
            }
            
        }
    }

    public void UniRxTimer()
    {
        Observable.Timer(System.TimeSpan.FromSeconds(GameSettings.Instance.GameData.ObservableTime)) 
        .Repeat() 
        .Subscribe(_ => { 
            Color color = new Color(Random.value, Random.value, Random.value, 255f);
            mesh.material.color = color;
        }).AddTo(disposables); 
    }

    void OnEnable()
    { 
        disposables = new CompositeDisposable();
    }

    void OnDisable()
    { 
        if (disposables != null)
            disposables.Dispose();
    }
}
