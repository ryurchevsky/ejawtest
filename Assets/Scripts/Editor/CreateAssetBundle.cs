﻿using UnityEditor;

public class CreateAssetBundle
{
    [MenuItem ("Asset Bundle/ Build Asset Bundle")]
    private static void BuildAssetBundle()
    {
        string path = EditorUtility.SaveFolderPanel("Save Bundle", "", "");

        if (path.Length != 0)
            BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.StandaloneWindows64);
    }
}
