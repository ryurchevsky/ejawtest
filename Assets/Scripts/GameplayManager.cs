﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    public List<GameObject> geometryList;
    private GameObject geometry;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            if (geometry == null)
                SpawnObject();
    }

    void SpawnObject()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            Vector3 wordPos = hit.point;
            geometry = Instantiate(RandomObjFromList(geometryList), wordPos, Quaternion.identity);    
        }
    }

    public GameObject RandomObjFromList(List<GameObject> list)
    {
        return list[Random.Range(0, list.Count)].gameObject;
    }
}
